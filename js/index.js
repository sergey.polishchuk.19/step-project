'use strict'

const buttonItem = document.querySelectorAll('.button-item'),
      tabsItems = document.querySelectorAll('.our-desc'),
      imgItem = document.querySelectorAll('.img-item');
buttonItem.forEach(element =>{
    element.addEventListener('click', () => {
        let currentBtn = element;
        let tabId = currentBtn.dataset['tabs'];
        let currentTab = document.querySelector(tabId);

        
        buttonItem.forEach(element=>{
            element.classList.remove('active')
        })
        tabsItems.forEach(element=>{
            element.classList.remove('active')
        })


        currentBtn.classList.add('active')
        currentTab.classList.add('active')
    })
})

function tabsElements (){
    const filterBox = document.querySelectorAll('.img-item'),
          btnItmes = document.querySelector('.btn-items'),
          btnLi = document.querySelectorAll('.tab');
    btnLi.forEach(element =>{
    element.addEventListener('click', ()=>{
        let currentBtn = element;
        btnLi.forEach(element =>{
            element.classList.remove('active-border');
        });
        currentBtn.classList.add('active-border');
    });
});
btnItmes.addEventListener('click', event =>{
    if(event.target.tagName !== 'LI')return false;
    let filterClass = event.target.dataset.filter;
    filterBox.forEach(elem => {
        elem.classList.remove('hide');
        if(!elem.classList.contains(filterClass) && filterClass !== 'all'){
            elem.classList.add('hide');
        }
    });
});
}
tabsElements()

const btnLoad = $('.load-btn'),
      items = $('.img-item.hidden-img');
btnLoad.on("click", event => {
    if (items.length === 12) $(event.target).remove();
        const itemsHidden = $.grep(items, (item, index) => {
    if (index < 12) return true});
    $(itemsHidden).removeClass("hidden-img");
});

let slideNow = 1,
    slideCount = $('#slidewrapper').children().length,
    slideInterval = 6000,
    navBtnId = 0,
    translateWidth = 0;

$(document).ready(function() {
    let switchInterval = setInterval(nextSlide, slideInterval);
    $('#viewport').hover(function() {
        clearInterval(switchInterval);
    }, function() {
        switchInterval = setInterval(nextSlide, slideInterval);
    });

    $('.next-btn').click(function() {
        nextSlide();
    });

    $('.prev-btn').click(function() {
        prevSlide();
    });


    $('.slide-nav-btn').click(function() {
        navBtnId = $(this).index();
        if (navBtnId + 1 != slideNow) {
            translateWidth = -$('#viewport').width() * (navBtnId);
            $('#slidewrapper').css({
                'transform': 'translate(' + translateWidth + 'px, 0)',
            });
            slideNow = navBtnId + 1;
        }
    });
});

function nextSlide() {
    if (slideNow == slideCount || slideNow <= 0 || slideNow > slideCount) {
        $('#slidewrapper').css('transform', 'translate(0, 0)');
        slideNow = 1;
    } else {
        translateWidth = -$('#viewport').width() * (slideNow);
        $('#slidewrapper').css({
            'transform': 'translate(' + translateWidth + 'px, 0)',
        });
        slideNow++;
    }
}

function prevSlide() {
    if (slideNow == 1 || slideNow <= 0 || slideNow > slideCount) {
        translateWidth = -$('#viewport').width() * (slideCount - 1);
        $('#slidewrapper').css({
            'transform': 'translate(' + translateWidth + 'px, 0)'
        });
        slideNow = slideCount;
    } else {
        translateWidth = -$('#viewport').width() * (slideNow - 2);
        $('#slidewrapper').css({
            'transform': 'translate(' + translateWidth + 'px, 0)'
        });
        slideNow--;
    }
}